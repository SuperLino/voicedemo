package com.gdkdemo.voice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.KeyEvent;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class VoiceDemoActivity extends Activity {

	public static final int SPEECH_REQUEST = 0;

	private TextView tv_screen_content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_voicedemo);

		tv_screen_content = (TextView) findViewById(R.id.screen_content);

	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			recognizeSpeech();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SPEECH_REQUEST && resultCode == RESULT_OK) {

			List<String> results = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS_PENDINGINTENT_BUNDLE);
			String spokenText = results.get(0);
			tv_screen_content.setText(spokenText);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void recognizeSpeech() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		startActivityForResult(intent, SPEECH_REQUEST);
	}

}
